#效果图

![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/200519_830915a1_312885.png "在这里输入图片标题")
![输入图片说明](http://git.oschina.net/uploads/images/2016/0504/200531_76fb3faf_312885.png "在这里输入图片标题")

#核心代码
```
/**
     * 实现ImageView的变色
     *
     * @param imageView
     * @param color
     */
    public static void setTint(@NonNull ImageView imageView, @ColorInt int color) {
        ColorStateList s1 = ColorStateList.valueOf(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imageView.setImageTintList(s1);
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (imageView.getDrawable() != null)
                imageView.getDrawable().setColorFilter(color, mode);
        }
    }
```

```
/**
     * 实现textView的Compound图标变色
     *
     * @param textView
     * @param color
     * @param isChangeTextColor 是否改变字的颜色
     */
    public static void setCompoundTint(@NonNull TextView textView, @ColorInt int color, boolean isChangeTextColor) {
        ColorStateList s1 = ColorStateList.valueOf(color);
        if (isChangeTextColor) {
            textView.setTextColor(s1);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            textView.setCompoundDrawableTintList(s1);
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            for (Drawable d : textView.getCompoundDrawables()) {
                if (d != null)
                    d.setColorFilter(color, mode);
            }

        }
    }
```

```
/**
     * @param button
     * @param color
     */
    public static void setTint(@NonNull Button button, @ColorInt int color) {
        ColorStateList s1 = ColorStateList.valueOf(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            button.setBackgroundTintList(s1);
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (button.getBackground() != null)
                button.getBackground().setColorFilter(color, mode);
        }
    }
```

```
/**
     * 处理SeekBar的变色
     *
     * @param seekBar
     * @param color
     */
    public static void setTint(@NonNull SeekBar seekBar, @ColorInt int color) {
        ColorStateList s1 = ColorStateList.valueOf(color);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            seekBar.setThumbTintList(s1);
            seekBar.setProgressTintList(s1);
        } else if (Build.VERSION.SDK_INT > Build.VERSION_CODES.GINGERBREAD_MR1) {
            Drawable progressDrawable = DrawableCompat.wrap(seekBar.getProgressDrawable());
            seekBar.setProgressDrawable(progressDrawable);
            DrawableCompat.setTintList(progressDrawable, s1);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Drawable thumbDrawable = DrawableCompat.wrap(seekBar.getThumb());
                DrawableCompat.setTintList(thumbDrawable, s1);
                seekBar.setThumb(thumbDrawable);
            }
        } else {
            PorterDuff.Mode mode = PorterDuff.Mode.SRC_IN;
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                mode = PorterDuff.Mode.MULTIPLY;
            }
            if (seekBar.getIndeterminateDrawable() != null)
                seekBar.getIndeterminateDrawable().setColorFilter(color, mode);
            if (seekBar.getProgressDrawable() != null)
                seekBar.getProgressDrawable().setColorFilter(color, mode);
        }
    }
```