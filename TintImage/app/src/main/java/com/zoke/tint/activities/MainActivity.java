package com.zoke.tint.activities;

import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.larswerkman.holocolorpicker.SaturationBar;
import com.larswerkman.holocolorpicker.ValueBar;
import com.zoke.tint.BaseActivity;
import com.zoke.tint.R;
import com.zoke.tint.helper.TintManager;

import org.xutils.view.annotation.ContentView;
import org.xutils.view.annotation.Event;
import org.xutils.view.annotation.ViewInject;

/**
 * @author 大熊 QQ:651319154
 */
@ContentView(R.layout.activity_main)
public class MainActivity extends BaseActivity implements ColorPicker.OnColorChangedListener, ColorPicker.OnColorSelectedListener {
    @ViewInject(R.id.toolbar)
    private Toolbar mToolbar;

    @ViewInject(R.id.themeIv)
    private ImageView themeIv;
    @ViewInject(R.id.themeTv)
    private TextView themeTv;
    @ViewInject(R.id.homeIv)
    private ImageView homeIv;
    @ViewInject(R.id.homeTv)
    private TextView homeTv;
    @ViewInject(R.id.frIv)
    private ImageView frIv;
    @ViewInject(R.id.frTv)
    private TextView frTv;

    @ViewInject(R.id.picker)
    private ColorPicker picker;

    @ViewInject(R.id.svbar)
    private SVBar svbar;
    @ViewInject(R.id.opacitybar)
    private OpacityBar opacitybar;
    @ViewInject(R.id.saturationbar)
    private SaturationBar saturationBar;
    @ViewInject(R.id.valuebar)
    private ValueBar valueBar;

    @ViewInject(R.id.btn1)
    private Button btn1;

    @ViewInject(R.id.btn2)
    private Button btn2;
    @ViewInject(R.id.t1)
    private TextView t1;
    @ViewInject(R.id.t2)
    private TextView t2;
    @ViewInject(R.id.seekBar1)
    private SeekBar seekBar1;
    @ViewInject(R.id.et1)
    private EditText et1;
    @ViewInject(R.id.et2)
    private EditText et2;
    @ViewInject(R.id.cb1)
    private CheckBox cb1;
    @ViewInject(R.id.pb1)
    private ProgressBar pb1;
    @ViewInject(R.id.progressBar)
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mToolbar.setTitle("图标变色");
        setSupportActionBar(mToolbar);
        //设置初始值
        picker.setColor(getResources().getColor(R.color.colorPrimary));
        picker.setOnColorChangedListener(this);
        picker.setOnColorSelectedListener(this);
        picker.addSVBar(svbar);
        picker.addOpacityBar(opacitybar);
        picker.addSaturationBar(saturationBar);
        picker.addValueBar(valueBar);
    }

    @Override
    public void onColorChanged(int color) {
        mToolbar.setBackgroundColor(color);
        getStatusBar().setBackgroundColor(color);
        TintManager.setTint(themeIv, color);
        TintManager.setTint(homeIv, color);
        TintManager.setTint(frIv, color);
        TintManager.setTint(btn1, color);
        TintManager.setTint(btn2, color);
        TintManager.setCompoundTint(t1, color, true);
        TintManager.setCompoundTint(t2, color, true);
        TintManager.setTint(et1, color);
        TintManager.setTint(et2, color);
        et2.setTextColor(color);
        TintManager.setTint(seekBar1, color);
        TintManager.setTint(cb1, color);
        themeTv.setTextColor(color);
        homeTv.setTextColor(color);
        frTv.setTextColor(color);
        TintManager.setTint(pb1, color);
        TintManager.setTint(progressBar, color);
        mToolbar.setTitle("图标变色:#" + Integer.toHexString(color));
    }

    @Override
    public void onColorSelected(int color) {

    }
}
