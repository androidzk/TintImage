package com.zoke.tint;

import android.app.Application;

import org.xutils.x;

/**
 * @author 大熊 QQ:651319154
 */
public class BaseApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        x.Ext.init(this);
        x.Ext.setDebug(BuildConfig.DEBUG);
    }
}
